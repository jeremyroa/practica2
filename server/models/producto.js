/**
 * @author Jeremy Roa
 * @date 10/12/2018
 * @description CRUD Supermercado
 */

const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

let Schema = mongoose.Schema;

let categoriasValidas = {
    values: ['EMBUTIDOS', 'CARNES','VEGETALES','FRUTAS'],
    message: '{VALUE} no es un rol válido'
};

let productoSchema = new Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es necesario'],
        unique: true
    },
    cantidad: {
        type: Number,
        required: [true, 'La cantidad es necesario']
    },
    precio: {
        type: Number,
        required: [true, 'El precio es obligatorio']
    },
    categoria: {
        type: String,
        enum: categoriasValidas,
        required: [true, 'El precio es obligatorio']
    },
    estado: {
        type: Boolean,
        default: true
    },
});

productoSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });

module.exports = mongoose.model('Producto', productoSchema);