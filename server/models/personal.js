/**
 * @author Jeremy Roa
 * @date 10/12/2018
 * @description CRUD Supermercado
 */

const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

let Schema = mongoose.Schema;

let generoValidar = {
    values: ['FEMENINO','MASCULINO'],
    message: '{VALUE} no es un rol válido'
};

let personalSchema = new Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es necesario'],
    },
    cedula: {
        type: Number,
        required: [true, 'La cedula es obligatoria'],
        unique: true

    },
    edad: {
        type: Number,
        required: [true, 'Edad es obligatoria']
    },
    genero: {
        type: String,
        enum: generoValidar,
        required: [true, 'Genero es obligatorio']
    },
    direccion: {
        type: String,
        required: [true, 'La direccion es obligatorio']
    },
    fnacimiento: {
        type: Date,
        required: [true, 'La fecha de nacimiento es necesaria']
    },
    usuario: {
        type: String,
        required: [true, 'El usuario es obligatorio'],
        unique: true
    },
    contrasenia: {
        type: String,
        required: [true, 'La contraseña es obligatoria'],
    },
    email: {
        type: String,
        required: [true, 'El email es obligatorio'],
        unique: true
    },


});

personalSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });

module.exports = mongoose.model('Personal', personalSchema);